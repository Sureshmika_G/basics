package basics.day72.collections.set;

import java.util.HashSet;
import java.util.Set;

public class SetTest2 {

	public static void main(String[] args) {

		Person1 bibinOne = new Person1("Bibin", 37);
		Person1 bibinTwo = new Person1("Bibin", 37);
		Person1 bibinThree = new Person1("Bibin", 37);

		Set<Person1> personSet = new HashSet<>();
		/**
		 * 1. Add the above objects into personSet
		 * 
		 * 2. Print the values
		 * 
		 * 3. Add changes (hint: equals() and hashCode() methods in Person1 class), so
		 * that duplicate elements will not be present in personSet after adding an
		 * element with same name and age
		 * 
		 * 4. Print toString() of all the three objects (without overriding toString()
		 * in Person1 class) before and after overriding equals() and hashCode() methods
		 * and observe the difference
		 */
	}

}
