package basics.day71.collections.list;

import java.util.ArrayList;
import java.util.List;

public class ListTest2 {

	public static void main(String[] args) {

		Person1 bibin = new Person1("Bibin", 37);
		Person1 ramesh = new Person1("Ramesh", 25);
		Person1 alex = new Person1("Alex", 50);

		List<Person1> personList = new ArrayList<>();
		/**
		 * 1. Add the above objects into personList
		 * 
		 * 2. Print the values
		 * 
		 * 3. Sort personList in the ascending order of name and print (Use Comparator
		 * and implement in a new class)
		 * 
		 * 4. Sort personList in the ascending order of age and print (Use Comparator
		 * and implement in this class itself)
		 * 
		 * 5. Implement sorting with Comparable instead of Comparator
		 * (https://www.baeldung.com/java-comparator-comparable)
		 * 
		 */
	}

}
