package basics.day61.oops.statickeyword;

public class Person1 {
	private String name;
	private int age;

	/**
	 * 1. Understand the usage of static keyword used with the class attribute
	 * country
	 * 
	 * 2. Try using either name or age inside getCountry() and setCountry() methods.
	 * Observe the behavior
	 */
	private static String country;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public static String getCountry() {
		return country;
	}

	public static void setCountry(String country) {
		Person1.country = country;
	}

}
