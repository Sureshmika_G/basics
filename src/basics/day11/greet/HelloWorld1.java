package basics.day11.greet;

public class HelloWorld1 {

	/**
	 * 1. Enhance this method to print your full name. Names are stored in two
	 * String variables within this method; one variable holds your first name, and
	 * the next your last name. Example output for this program is - Hello World,
	 * Bibin Joseph
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String firstName="Bibin";
		String lastName=" Joseph";
		String fullName=firstName+lastName;
		System.out.println("Hello World,"+ fullName);
	}
}
