package basics.day21.datatypes;

public class StringOperations2 {

	/**
	 * All of these should be implemented as separate private methods (refer
	 * printFullNameInUpperCase() below for reference)
	 * 
	 * 1. Display your full name in upper case (logic already added here for your
	 * reference)
	 * 
	 * 2. Display your full name in lower case
	 * 
	 * 3. Display the length of your first name
	 * 
	 * 4. Display the length of your last name
	 * 
	 * 5. Display the length of your full name
	 * 
	 * 6. Display the 3rd character of your first name (hint: use chatAt() method
	 * available in String class)
	 * 
	 * 7. Check if an input string contains a particular word. For example, if the
	 * input string is "This is a sample string" and the word whose presence is to
	 * be checked is "sample", then the output should be "The word sample is present
	 * in the given input". If it is not present, then print appropriate message
	 * (hit: use contains() method available in String class)
	 * 
	 * 8. Replace a word in an input string with another word. For example, the
	 * input string is "Change your name" and the word to be replaced is "your" and
	 * it needs to be replaced with "my", then the output should be "Change my name"
	 * (hint: use replaceAll() method available in String class)
	 * 
	 * 9. Add a method to check if two strings are equal.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String firstName = "bibin";
		String lastName = "joseph";
		printFullNameInUpperCase(firstName, lastName);
	}

	private static void printFullNameInUpperCase(String firstName, String lastName) {
		System.out.println("Full name in upper case: " + (firstName.concat(" ").concat(lastName).toUpperCase()));
	}

}
