package basics.day22.datatypes.arrays;

public class StringArray3 {

	/**
	 * 1. Split an input string on character comma (,) and store them in a String[].
	 * (hint: use the split() method available in String class). Print the values of
	 * all elements after removing the leading and trailing spaces in them. Example
	 * input: " This, is, a, sample, input, string "
	 * 
	 * 2. Enhance the above logic (write as a new method) to split the input string
	 * on space (instead of comma) and eliminate duplicate elements from the output
	 * array. Example input "This is sample string where the string sample is coming
	 * in more than one place"
	 * 
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String inputString = "  This,          is, a, sample,                string";
		String[] splitString = inputString.split(",");
		System.out.println(splitString.length);
	}

}
