package basics.day81.collections.map;

public class MapTest {

	public static void main(String[] args) {
		/**
		 * 1. Create 5 objects for Person1 and set all attributes including address
		 * 
		 * 2. Add these 5 objects into a Map (using put() method available in Map). Key
		 * is person's name and value is Peron1's object
		 * 
		 * 3. Override toString() in Person1 and Address2 to display all necessary
		 * attributes
		 * 
		 * 4. Iterate the map and print all elements (key as well as value). Refer
		 * https://www.geeksforgeeks.org/iterate-map-java/
		 * 
		 * 5. Accept a person name from input and fetch its value from the Map and print
		 * the details
		 * 
		 * 6. Print "The object is not found" if user provided a non-existing key (there
		 * are built-in methods available in Map to check if a key is present or not)
		 * 
		 * 7. Use put() method to insert objects with same key multiple times and check
		 * how many elements were actually inserted into the map
		 */

	}

}
