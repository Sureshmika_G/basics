package basics.day63.oops.superkeyword;

public class Calculator1Impl2 extends Calculator1 {

	@Override
	void add(int a, int b) {
		/**
		 * 1. Observe the behavior (when run SuperTest3) with and without super.add() in
		 * this class
		 */
		super.add(a, b);
		System.out.println("From CalculatorImpl " + (a + b));
	}
}
