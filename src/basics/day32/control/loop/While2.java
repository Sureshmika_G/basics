package basics.day32.control.loop;

import java.time.LocalDateTime;

public class While2 {

	/**
	 * 1. Display random numbers between 1 and 100. Stop loop if the generated
	 * number is 50 (hint: use int randomNumber = new SecureRandom().nextInt(100);).
	 * Following is an example for reference which displays the time until it
	 * reaches current time
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		// Edit the below variable stopAtMinute to a suitable recent minute when you run
		// this program
		int stopAtMinute = 59;

		int currentHour = 0;
		int currentMinute = 0;
		int currentSecond = 0;
		int secondToDisplay = 0;

		while (currentMinute != stopAtMinute) {
			LocalDateTime currentTime = LocalDateTime.now();
			currentHour = currentTime.getHour();
			currentMinute = currentTime.getMinute();
			currentSecond = currentTime.getSecond();
			if (secondToDisplay != currentSecond) {
				secondToDisplay = currentSecond;
				System.out.println(currentHour + ":" + currentMinute + ":" + secondToDisplay);
			}
		}
	}

}
